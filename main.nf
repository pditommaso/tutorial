#!/usr/bin/env nextflow

cheers = Channel.from 'Bojour', 'Ciao', 'Hello', 'Hola'

process sayHello {
  echo true
  input: 
  val x from cheers
  
  """
  echo '$x world!'
  """
}
